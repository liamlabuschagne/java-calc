# java-calc
This project is a fun little experiment in recursive logic in order to parse mathematical expressions.
This calculator adheres to BEDMAS and supports all the operators therein including nested expressions inside of brackets.

## Supported operations
B - Brackets `(5 + 2) * 4 = 28`

E - Exponents `5 ^ 2 = 25`

D - Division `4 / 2 = 2`

M - Multiplication `2 * 4 = 8`

% - Modulus `5 % 4 = 1`

A - Addition `5 + 2 = 7`

S - Subtraction `5 - 2 = 3`

## Compilation & Installation
**Note**: The OpenJDK package is required for compilation as it provides the `javac` command
1. Clone this repository into your installation directory (can be anywhere) 
1. Compile: `javac Calc.java`
1. Add installation directory to path environment variable

    Edit ~/.bashrc:
    ```
    export PATH=[INSTALLATION DIRECTORY]:${PATH}
    ```

1. Change classpath to installation directory in `java-calc` script:

    ```
    java -classpath [INSTALLATION DIRECTORY] Calc "$1"
    ```
1. (Optional) Add an alias for even more convenient access:

    Edit ~/.bashrc
    ```
    alias calc=java-calc
    ```

## Usage
Simply run `java-calc`, then enter your mathematical expression.