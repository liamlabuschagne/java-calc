import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Calc {

    public static void main(String[] args) {
        // Get the expression from the input
        System.out.println("Enter expression:");
        String expression = System.console().readLine();

        if (Pattern.matches("[^\\(\\^/\\*%\\+\\-\\d\\.E]", expression) || expression.isEmpty()) {
            System.out.println("Invalid input. Valid characters are: ()^/*+-.E and digits 0-9");

            return;
        }
        // Remove any whitespace or new lines
        expression = expression.replaceAll("\\s", "");

        System.out.println(compute(expression));
    }

    public static double compute(String expression) {

        // If it's an empty expression, return 0
        if (expression.isEmpty()) {
            return 0;
        }

        if (expression.equals("Infinity")) {
            System.out.println("Answer too big");
            return 0;
        }

        // If computing a number with no operation, return the number
        if (Pattern.matches("[\\d\\.E]+", expression)
                | (Pattern.matches("[\\d\\.E\\-]+", expression) && expression.charAt(0) == '-')) {
            return Double.parseDouble(expression);
        }

        int openBracketPos = 0;
        int openBracketCount = 0;
        int closeBracketPos = 0;
        // Check for brackets
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(') {
                if (openBracketCount == 0) {
                    openBracketPos = i;
                }
                openBracketCount++;
            } else if (expression.charAt(i) == ')') {
                if (openBracketCount == 1) {
                    closeBracketPos = i;
                    break;
                }
                openBracketCount--;
            }
        }

        if (openBracketCount == 1) {
            // Isolated bracketed term
            String bracketedTerm = expression.substring(openBracketPos + 1, closeBracketPos);

            // Remove bracketed section from the rest of the expression
            return compute(expression.substring(0, openBracketPos) + compute(bracketedTerm)
                    + expression.substring(closeBracketPos + 1));
        }

        // Find all the operators
        List<Integer> operatorLocations = getOperatorLocations(expression);

        if (operatorLocations.size() > 1) {
            // Isolate operation to do first
            String doFirstExpression = expression.substring(operatorLocations.get(operatorLocations.size() - 2) + 1);

            // Get the expression without this in it
            expression = expression.substring(0, operatorLocations.get(operatorLocations.size() - 2) + 1);

            return compute(expression + compute(doFirstExpression));
        }

        int alength = getNextNumberLength(expression);
        double a = Double.parseDouble(expression.substring(0, alength));
        expression = expression.substring(alength);

        char op = expression.charAt(0);
        expression = expression.substring(1);

        int blength = getNextNumberLength(expression);
        double b = Double.parseDouble(expression.substring(0, blength));
        expression = expression.substring(blength);

        double result = compute(a, op, b);

        return compute(Double.toString(result) + expression);
    }

    public static int getNextNumberLength(String expression) {
        int length = 0;

        for (int i = 0; i < expression.length(); i++) {
            // Get the next single character
            String c = expression.substring(i, i + 1);

            if (Pattern.matches("\\d|\\.|E", c)) {
                // If the character is a digit (0-9) or a period (.) then it is part of the
                // number
                length++;
            } else {
                // The number has ended, we're done
                break;
            }
        }

        return length;
    }

    public static List<Integer> getOperatorLocations(String expression) {
        char[] operators = { '^', '/', '*', '%', '+', '-' };
        int[] operatorRanks = { 2, 1, 1, 1, 0, 0 };

        int highestRankFound = -1;

        List<Integer> operatorLocations = new ArrayList<>();

        // Find all the operators
        for (int i = 0; i < expression.length(); i++) {
            // Get the next single character
            String c = expression.substring(i, i + 1);

            if (Pattern.matches("\\^|/|\\*|%|\\+|\\-", c)) {

                // Find out which operator it is
                for (int j = 0; j < operators.length; j++) {
                    // Use this to figure out it's rank
                    if (operators[j] == c.charAt(0) && operatorRanks[j] > highestRankFound) {
                        highestRankFound = operatorRanks[j];
                        operatorLocations.add(i);
                    }
                }
            }
        }

        return operatorLocations;
    }

    public static double compute(double a, char op, double b) {

        double result = 0;
        switch (op) {
            case '+':
                result = a + b;
                break;
            case '-':
                result = a - b;
                break;
            case '*':
                result = a * b;
                break;
            case '/':
                if (b == 0) {
                    System.out.println("Can't divide by zero");
                } else
                    result = a / b;
                break;
            case '^':
                result = Math.pow(a, b);
                break;
            case '%':
                result = a % b;
                break;
            default:
                System.out.println("Invalid operator: " + op);
                break;
        }
        return result;
    }
}
